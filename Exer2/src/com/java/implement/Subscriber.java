package com.java.implement;

public abstract class Subscriber {

	protected NotificationPublisher notif;
	public abstract void onUpdate();
}
