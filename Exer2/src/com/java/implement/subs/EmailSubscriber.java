package com.java.implement.subs;

import com.java.implement.NotificationPublisher;
import com.java.implement.Subscriber;

public class EmailSubscriber extends Subscriber implements Runnable{

	public EmailSubscriber(NotificationPublisher notif) {
		super.notif = notif;
	}
	
	@Override
	public void onUpdate() {
		System.out.println("Email Notif Receive: " + super.notif.getMessage());
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		onUpdate();
	}

}
