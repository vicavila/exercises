package com.java.implement.subs;

import com.java.implement.NotificationPublisher;
import com.java.implement.Subscriber;

public class SMSSubscriber extends Subscriber implements Runnable {

	public SMSSubscriber(NotificationPublisher notif) {
		super.notif = notif;
	}
	
	@Override
	public void onUpdate() {
		
		System.out.println("SMS Notif Receive: " + super.notif.getMessage());
	}

	@Override
	public void run() {
		onUpdate();
	}

}
