package com.java.implement;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class NotificationPublisher {

	private final Set<Subscriber> subscribers = Collections
			.newSetFromMap(new ConcurrentHashMap<Subscriber, Boolean>(0));

	private String message;

	public void register(Subscriber subscriber) {
		subscribers.add(subscriber);
	}

	public void unRegister(Subscriber subscriber) {
		subscribers.remove(subscriber);
	}

	private void notifySubscribers() {

		  subscribers.forEach(sub -> sub.onUpdate());
	  }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
		
		notifySubscribers();
	}
}
