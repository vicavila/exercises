package com.java.runner;

import com.java.implement.NotificationPublisher;
import com.java.implement.Subscriber;
import com.java.implement.subs.EmailSubscriber;
import com.java.implement.subs.SMSSubscriber;

public class Main {

	public static void main(String[] args) {

		NotificationPublisher notif = new NotificationPublisher();

		Subscriber sms = new SMSSubscriber(notif);
		Subscriber email = new EmailSubscriber(notif);

		notif.register(sms);
		notif.register(email);

		for (int i = 1; i <= 5; i++) {
			notif.setMessage("Message " + i);
		}

	}
}
