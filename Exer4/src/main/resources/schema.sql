CREATE TABLE IF NOT EXISTS user (
 id INTEGER NOT NULL AUTO_INCREMENT,
 first_name VARCHAR(50),
 last_name VARCHAR(50),
 account_balance DECIMAL(10,2),
 PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS transaction (
 id INTEGER NOT NULL AUTO_INCREMENT,
 amount DECIMAL(10,2),
 label VARCHAR(10),
 current_balance DECIMAL(10,2),
 user_id INTEGER,
 other_id INTEGER,
 PRIMARY KEY(id)
);