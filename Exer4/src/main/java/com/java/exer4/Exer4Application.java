package com.java.exer4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.java.exer4.users.model.User;
import com.java.exer4.users.service.UserService;

@SpringBootApplication
public class Exer4Application implements CommandLineRunner {

	@Autowired
	private UserService userService;
	
	public static void main(String[] args) {
		SpringApplication.run(Exer4Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		addUsers();
	}

	private void addUsers() {
		
		User user = new User();
		user.setFirstName("John");
		user.setLastName("Doe");
		user.setAccountBalance(1000);
		userService.addUser(user);
		
		user = new User();
		user.setFirstName("Anna");
		user.setLastName("Smith");
		user.setAccountBalance(2000);
		userService.addUser(user);
		
		user = new User();
		user.setFirstName("Mark");
		user.setLastName("Jones");
		user.setAccountBalance(3000);
		userService.addUser(user);
	}

}
