package com.java.exer4.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.java.exer4.users.model.TransactionHistory;
import com.java.exer4.users.model.User;
import com.java.exer4.users.service.UserService;

@RestController
@RequestMapping("/api/v1/users")
public class UsersController {

	@Autowired
	private UserService userService;

	@GetMapping
	public ResponseEntity<Object> getAllUsers() {

		return ResponseEntity.ok(userService.getAllUsers());
	}

	@GetMapping("/{id}")
	public ResponseEntity<Object> getUserInfo(@PathVariable("id") int id) {

		User user = userService.getUserInfo(id);
		if(user != null)
			return ResponseEntity.ok(user);
		
		String msg = "Error: User not found.";
		return new ResponseEntity<>("{\"message\": \""+ msg +"\" }", HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/{id}/transactions")
	public ResponseEntity<Object> getUserUserTransactions(@PathVariable("id") int id) {

		return ResponseEntity.ok(userService.getUserUserTransactions(id));
	}

	@PostMapping("/{id}/deposit")
	public ResponseEntity<Object> deposit(@PathVariable("id") int id, @RequestBody TransactionHistory deposit) {

		userService.deposit(id, deposit);
		
		return ResponseEntity.ok().build();
	}

	@PostMapping("/{id}/withdraw")
	public ResponseEntity<Object> withdraw(@PathVariable("id") int id, @RequestBody TransactionHistory withdraw) {

		userService.withdraw(id, withdraw);
		
		return ResponseEntity.ok().build();
	}
	
	@PostMapping("/{id}/transfer")
	public ResponseEntity<Object> transfer(@PathVariable("id") int id, @RequestBody TransactionHistory transfer) {

		userService.transfer(id, transfer);
		
		return ResponseEntity.ok().build();
	}
}
