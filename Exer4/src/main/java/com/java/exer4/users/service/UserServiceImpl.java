package com.java.exer4.users.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.exer4.users.mapper.UserMapper;
import com.java.exer4.users.model.TransactionHistory;
import com.java.exer4.users.model.User;

@Service
public class UserServiceImpl implements UserService {
	
	private static final String DEPOSIT = "DEPOSIT";

	private static final String WITHDRAW = "WITHDRAW";
	
	private static final String TRANSFER = "TRANSFER";
	
	private static final String RECEIVE = "RECEIVE";

	@Autowired
	private UserMapper userMapper;

	@Override
	public List<User> getAllUsers() {

		return userMapper.getAll();
	}

	@Override
	public User getUserInfo(int id) {

		return userMapper.getOne(id);
	}

	@Override
	public void deposit(int id, TransactionHistory deposit) {

		validateAmount(deposit.getAmount());
		
		User user = validateUser(id);
		
		double userBal = user.getAccountBalance() + deposit.getAmount();
		
		userMapper.updateBalance(id, userBal);
		userMapper.withdrawOrDeposit(id, deposit.getAmount(), userBal, DEPOSIT);
	}

	@Override
	public void withdraw(int id, TransactionHistory withdraw) {

		validateAmount(withdraw.getAmount());
		
		User user = validateUser(id);
		
		checkBalance(user.getAccountBalance(), withdraw.getAmount());
		
		double userBal = user.getAccountBalance() - withdraw.getAmount();
		
		userMapper.updateBalance(id, userBal);
		userMapper.withdrawOrDeposit(id, withdraw.getAmount(), userBal, WITHDRAW);
	}

	@Override
	public void transfer(int id, TransactionHistory transfer) {

		validateAmount(transfer.getAmount());
		
		User user = validateUser(id);
		
		User recipient = validateRecipient(transfer.getOtherId());
		
		checkBalance(user.getAccountBalance(), transfer.getAmount());
		
		double userBal = user.getAccountBalance() - transfer.getAmount();
		double recipBal = recipient.getAccountBalance() + transfer.getAmount();
		
		// UPDATE SENDER
		userMapper.updateBalance(id, userBal);
		userMapper.transferOrReceive(id, transfer.getAmount(), userBal, TRANSFER, transfer.getOtherId());
		
		// UPDATE RECIPIENT
		userMapper.updateBalance(transfer.getOtherId(), recipBal);
		userMapper.transferOrReceive(transfer.getOtherId(), transfer.getAmount(), recipBal, RECEIVE, id);
	}

	@Override
	public List<TransactionHistory> getUserUserTransactions(int id) {

		return userMapper.getTransactions(id);
	}

	@Override
	public void addUser(User user) {

		userMapper.addUser(user);

	}

	private User validateUser(int id) {

		User user = userMapper.getOne(id);
		if(user == null)
			throw new IllegalArgumentException("Error: User does not exist.");
		
		return user;
	}

	private void validateAmount(double amount) {

		if(amount < 0)
			throw new IllegalArgumentException("Error: Amount should be greater than 0.");
	}

	private void checkBalance(double accountBalance, double amount) {

		if(accountBalance < amount)
			throw new IllegalArgumentException("Error: Account balance is running low.");
	}

	private User validateRecipient(int recipientId) {

		User recipient = userMapper.getOne(recipientId);
		if(recipient == null)
			throw new IllegalArgumentException("Error: Recipient User does not exist.");
		
		return recipient;
	}

}
