package com.java.exer4.users.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.java.exer4.users.model.TransactionHistory;
import com.java.exer4.users.model.User;

@Mapper
public interface UserMapper {

	@Insert("INSERT INTO user (first_name, last_name, account_balance) VALUES "
			+ "(#{firstName}, #{lastName}, #{accountBalance})")
	void addUser(User user);

	@Select("SELECT * FROM user ORDER BY last_name ASC")
	List<User> getAll();

	@Select("SELECT * FROM user WHERE id = #{userId}")
	User getOne(int userId);

	@Select("SELECT * FROM transaction "
			+ "WHERE user_id = #{userId} "
			+ "ORDER BY id DESC")
	List<TransactionHistory> getTransactions(int userId);

	@Update("UPDATE user SET account_balance = #{amount} "
			+ "WHERE id = #{userId}")
	void updateBalance(int userId, double amount);

	@Insert("INSERT INTO transaction (amount, label, current_balance, user_id, other_id) VALUES "
			+ "(#{amount}, #{label}, #{balance}, #{userId}, #{otherId})")
	void transferOrReceive(int userId, double amount, double balance, String label, int otherId);

	@Insert("INSERT INTO transaction (amount, label, current_balance, user_id) VALUES "
			+ "(#{amount}, #{transactionLabel}, #{balance}, #{userId})")
	void withdrawOrDeposit(int userId, double amount, double balance, String transactionLabel);
	
}
