package com.java.exer4.users.service;

import java.util.List;

import com.java.exer4.users.model.TransactionHistory;
import com.java.exer4.users.model.User;

public interface UserService {

	List<User> getAllUsers();

	User getUserInfo(int id);

	void deposit(int id, TransactionHistory deposit);

	void withdraw(int id, TransactionHistory withdraw);

	void transfer(int id, TransactionHistory transfer);

	void addUser(User user);

	List<TransactionHistory> getUserUserTransactions(int id);

}
