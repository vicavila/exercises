package com.java.runner;

public class Main {

	public static void main(String[] args) {
		
		PaymentOption cod = new CODPaymentOption();
		PaymentOption mcard = new MasterCardPaymentOption();
		PaymentOption gcash = new GCashPaymentOption();
		
		cod.payment();
		mcard.payment();
		gcash.payment();
	}
}
