package com.java.runner;

public class MasterCardPaymentOption implements PaymentOption {

	@Override
	public void payment() {
		System.out.println("Enter 16 digit card number and expiration for Master Card payment.");
	}

}
