package com.java.exer3.rates.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.exer3.mapper.ExchangeRatesMapper;
import com.java.exer3.rates.ExchangeRateAPI;
import com.java.exer3.rates.ExchangeRateAPIModel;
import com.java.exer3.util.Validator;

@Service
public class RateServiceImpl  implements RatesService{

	@Autowired
	private Validator validator;
	
	@Autowired
	private ExchangeRatesMapper ratesMapper;
	
	@Autowired
	private ExchangeRateAPI erApi;
	
	@Override
	public void fetchBaseLatest(String base, String symbols) {

		validator.validateRequired(base, "base");
		
		ExchangeRateAPIModel fetchResult = erApi.fetchExhangeRate(base, symbols);
		
		saveRatesToDB(fetchResult);
	}

	

	@Override
	public void fetchBaseHistory(String base, String date, String symbols) {

		validator.validateRequired(base, "base");
		validator.validateRequired(date, "date");
		
		ExchangeRateAPIModel fetchResult = erApi.fetchExhangeRate(base, date, symbols);

		saveRatesToDB(fetchResult);
	}

	private void saveRatesToDB(ExchangeRateAPIModel result) {

		if(result == null)
			throw new IllegalArgumentException("Error: Could be an incorrect arguments passed that the API could not fetch data.");
		
		
		// INSERT EXCHANGE RATES: (base, symbol, rate, date) 
		result.getRates().entrySet()
			.stream()
			.filter(set -> !set.getKey().equals(result.getBase()))
			.filter(set -> !ratesMapper.isDataExists(result.getBase(), set.getKey(), result.getDate()))
//			.forEach(set -> System.err.println(result.getBase() + ","+ set.getKey() + ", " + result.getDate()+ " : " + set.getValue()));
			.forEach(set -> ratesMapper.insertIfNotExists(
					result.getBase(), set.getKey(), set.getValue(), result.getDate()));
		
		
	}

	
}
