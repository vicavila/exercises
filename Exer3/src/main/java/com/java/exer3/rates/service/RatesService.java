package com.java.exer3.rates.service;

public interface RatesService {


	void fetchBaseLatest(String base, String symbols);

	void fetchBaseHistory(String base, String date, String symbols);

}
