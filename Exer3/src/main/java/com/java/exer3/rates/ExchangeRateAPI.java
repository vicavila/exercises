package com.java.exer3.rates;

import java.io.IOException;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class ExchangeRateAPI {

	private static final CloseableHttpClient httpclient = HttpClients.createDefault();
	private static final String BASE_API_URL = "https://api.exchangeratesapi.io/";

	public ExchangeRateAPIModel fetchExhangeRate(String base, String symbols) {

		String url = String.format(BASE_API_URL + "latest?base=%s", base);
		if(symbols != null)
			url = String.format(BASE_API_URL + "latest?base=%s&symbols=%s", base, symbols);

		return fetchRates(url);
	}

	public ExchangeRateAPIModel fetchExhangeRate(String base, String date, String symbols) {

		String url = String.format(BASE_API_URL + "%s?base=%s", date, base);
		if(symbols != null)
			url = String.format(BASE_API_URL + "%s?base=%s&symbols=%s", date, base, symbols);

		return fetchRates(url);
	}

	private ExchangeRateAPIModel fetchRates(String url) {

		HttpGet httpget = new HttpGet(url);
		httpget.setHeader("Accept", "application/json");
		httpget.setHeader("Content-type", "application/json");

		try (CloseableHttpResponse response = httpclient.execute(httpget)) {
			if (response.getStatusLine().getStatusCode() == 200) {
				try {

					return new ObjectMapper()
							.readValue(
									response.getEntity().getContent(),
									ExchangeRateAPIModel.class);

				} catch (IOException e) {
					e.printStackTrace(); // TODO: ADD PROPER LOGGER
				}

			}
		} catch (IOException e) {
			e.printStackTrace(); // TODO: ADD PROPER LOGGER
		}
		return null;
	}

}
