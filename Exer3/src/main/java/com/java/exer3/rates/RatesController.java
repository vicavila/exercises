package com.java.exer3.rates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.exer3.rates.service.RatesService;

/**
 * This controller class is responsible for getting Exchange Rates from outside API
 * @author v.avila
 *
 */
@RestController
@RequestMapping("/api/v1/rates")
public class RatesController {
	
	@Autowired
	private RatesService ratesService;

	/**
	 * Fetches all latest rates based on the given base symbol.
	 * @return
	 */
	@GetMapping("/{base}")
	public ResponseEntity<Object> getRatesLatest(
			@PathVariable("base") String base,
			@RequestParam(value = "symbols", required = false) String symbols){
		
		ratesService.fetchBaseLatest(base, symbols);
		
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/{base}/{date}")
	public ResponseEntity<Object> getRatesHistory(
			@PathVariable("base") String base,
			@PathVariable("date") String date,
			@RequestParam(value = "symbols", required = false) String symbols){
		
		ratesService.fetchBaseHistory(base, date, symbols);
		
		return ResponseEntity.ok().build();
	}
}
