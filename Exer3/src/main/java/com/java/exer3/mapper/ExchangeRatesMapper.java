package com.java.exer3.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.java.exer3.exchangerates.ExchangeRateModel;

@Mapper
public interface ExchangeRatesMapper {


	@Insert("INSERT INTO exchange_rates (base, symbol, rate, rate_date) VALUES "
			+ "(#{base}, #{symbol}, #{rate}, #{rateDate})")
	void insertIfNotExists(String base, String symbol, Double rate, String rateDate);

	@Select("SELECT * FROM exchange_rates "
			+ "ORDER BY base ASC, symbol ASC")
	List<ExchangeRateModel> fetchAllRates();

	@Select("SELECT * FROM exchange_rates "
			+ "WHERE base = #{base} "
			+ "ORDER BY base ASC, symbol ASC")
	List<ExchangeRateModel> fetchAllByBaseSymbol(String base);


	@Select("SELECT * FROM exchange_rates "
			+ "WHERE rate_date = #{date} "
			+ "ORDER BY base ASC, symbol ASC")
	List<ExchangeRateModel> fetchAllByDate(String date);

	@Select("SELECT * FROM exchange_rates "
			+ "WHERE base = #{base} AND rate_date = #{date} "
			+ "ORDER BY base ASC, symbol ASC")
	List<ExchangeRateModel> fetchByBaseSymbolByDate(String base, String date);

	@Select("SELECT EXISTS (SELECT id FROM exchange_rates "
			+ "WHERE base = #{base} AND symbol = #{symbol} AND rate_date = #{date})")
	boolean isDataExists(String base, String symbol, String date);

	
}
