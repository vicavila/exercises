package com.java.exer3.util;

import org.springframework.stereotype.Component;

@Component
public class Validator {

	public void validateRequired(String field, String value) {

		if(value == null || value.trim().length() == 0)
			throw new IllegalArgumentException(String.format("Error: '%s' is required.", field));
	}
}
