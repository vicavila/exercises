package com.java.exer3.exchangerates.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.exer3.exchangerates.ExchangeRateModel;
import com.java.exer3.mapper.ExchangeRatesMapper;

@Service
public class ExchangeRateServiceImpl implements ExchangeRateService {

	@Autowired
	private ExchangeRatesMapper erMapper;
	
	@Override
	public List<ExchangeRateModel> getExchangeRates(String base, String date) {

		// FETCH ALL RATES ANY DATE
		if(base == null && date == null)
			return erMapper.fetchAllRates();
		
		// FETCH RATES OF GIVEN BASE SYMBOL FOR THE CURRENT DATE 
		if(base != null && date == null)
			return erMapper.fetchAllByBaseSymbol(base);
		
		// FETCH ALL RATES BY GIVEN DATE
		if(base == null && date != null)
			return erMapper.fetchAllByDate(date);
		
		// FETCH RATES OF GIVEN BASE SYMBOL AND DATE
		if(base != null && date != null)
			return erMapper.fetchByBaseSymbolByDate(base, date);
		
		throw new IllegalArgumentException("Error: Invalid query param passed in fetching exchanges rates from DB.");
	}

}
