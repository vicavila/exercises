package com.java.exer3.exchangerates.service;

import java.util.List;

import com.java.exer3.exchangerates.ExchangeRateModel;

public interface ExchangeRateService {


	List<ExchangeRateModel> getExchangeRates(String base, String date);

}
