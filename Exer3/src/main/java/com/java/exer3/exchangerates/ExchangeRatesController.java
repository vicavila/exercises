package com.java.exer3.exchangerates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.exer3.exchangerates.service.ExchangeRateService;

/**
 * This controller class is responsible for displaying Exchanges Rates stored from our DB.
 * @author v.avila
 *
 */
@RestController
@RequestMapping("/api/v1/exchange-rates")
public class ExchangeRatesController {

	@Autowired
	private ExchangeRateService erService;
	
	@GetMapping
	public ResponseEntity<Object> getAllLatest(
			@RequestParam(value="base", required = false) String base,
			@RequestParam(value="date", required = false) String date) {
		
		return ResponseEntity.ok(erService.getExchangeRates(base, date));
	}
	
}
