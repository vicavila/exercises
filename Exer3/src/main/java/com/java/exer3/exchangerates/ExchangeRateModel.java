package com.java.exer3.exchangerates;

public class ExchangeRateModel {

	private String base;
	private String symbol;
	private double rate;
	private String rateDate;

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public String getRateDate() {
		return rateDate;
	}

	public void setRateDate(String rateDate) {
		this.rateDate = rateDate;
	}

	@Override
	public String toString() {
		return "ExchangeRateModel [base=" + base + ", symbol=" + symbol + ", rate=" + rate + ", rateDate=" + rateDate
				+ "]";
	}

}
